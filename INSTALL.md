## Premake Install Instructions
```
cd $PREMAKE_PATH/premake-core
make -f Bootstrap.mak linux
```


## Simulator Build Instructions 

```
cd ~/marble_run_simulator/
$PREMAKE_PATH/premake-core/bin/release/premake5 gmake
cd Build/gmake
make
```


### NOTE: Update these paths to the appropriate directories
 Line 28, MarbleRun/Tests/MarbleRunDemo.h - std::string tracks_path = "/home/aallaire/marble_run_simulator/MarbleRun/Data/tracks/";
 
 Line 64, MarbleRun/Framework/Main.cpp - std::string data_path = "/home/aallaire/marble_run_simulator/MarbleRun/Data/";
 
 Line 100,	MarbleRun/Framework/Main.cpp - const char* fontPath = "/home/aallaire/marble_run_simulator/MarbleRun/Data/DroidSans.ttf";

## How to Run

`cd ~/marble_run_simulator/MarbleRun`

Specify path to demo data (must have "/" at the end) and parameters (gravity, friction, restitution)

`../Build/gmake/bin/Debug/MarbleRun ~/marble_run_simulator/MarbleRun/Data/demos/complete/ -730 0.0004 0.3`

or use default arguments

`../Build/gmake/bin/Debug/MarbleRun`
