import os
import subprocess
import sys
import time
from scipy import interpolate
import numpy as np
import scipy
from scipy.optimize import differential_evolution, basinhopping
from matplotlib import pyplot as plt
from cycler import cycler

def get_prog_path_name(name):
  script_path = os.path.abspath(os.path.dirname(__file__))
  script2prog = os.path.join("Build", "gmake", "bin", "Debug", name)
  return os.path.join(script_path, script2prog)

def get_traj_file():
  file_path = "ball_trajectory.txt"
  return os.path.join(demo_path, file_path)

def get_p2m_ratio_file():
  script_path = os.path.abspath(os.path.dirname(__file__))
  file_path = "MarbleRun/Data/p2m_ratio.txt"
  return os.path.join(script_path, file_path)

def get_frame_size_file():
  file_path ="frame_size.txt"
  return os.path.join(demo_path, file_path)

def get_track_config_file():
  file_path =  "track_config.txt"
  return os.path.join(demo_path, file_path)

def get_demo_path(file_path):
  script_path = os.path.abspath(os.path.dirname(__file__))
  if script_path in file_path:
    ret_path = file_path
  else:
    ret_path = os.path.join(script_path, file_path)
  return ret_path

def get_contour_warp_path():
  file_path =  "contour_warps/contour"
  return os.path.join(demo_path, file_path)

def get_prog_path():
  return get_prog_path_name("MarbleRun")

def get_lib_path():
  from sys import platform
  return get_prog_path_name("libMarbleRun_lib" + ('.dylib' if platform == "darwin" else '.so'))

def run_prog_process(args):
  return list(map(float, subprocess.check_output([get_prog_path()] + args).strip().split()))

import ctypes
#prog_lib = ctypes.cdll.LoadLibrary(get_lib_path())
prog_lib = ctypes.CDLL(get_lib_path())

def run_prog_lib(args):
  argc = len(args) + 1
  argv_l = ["asdf"] + args

  argv = (ctypes.POINTER(ctypes.c_char) * (argc + 1))()
  for i, arg in enumerate(argv_l):
    argv[i] = ctypes.create_string_buffer(arg.encode())

  prog_lib.run_optimization_step.restype = ctypes.POINTER(ctypes.c_float)
  rets = prog_lib.run_optimization_step(argc, argv)
  size = int(round(np.ctypeslib.as_array(rets, shape=(1,))[0]))
  sim_trajectory_in = np.frombuffer((ctypes.c_float * (size + 1)).from_address(ctypes.addressof(rets.contents)), np.float32).copy()
  return sim_trajectory_in[1:]

def run_prog_lib_sim(args):
  argc = len(args) + 1
  argv_l = ["asdf"] + args

  argv = (ctypes.POINTER(ctypes.c_char) * (argc + 1))()
  for i, arg in enumerate(argv_l):
    argv[i] = ctypes.create_string_buffer(arg.encode())

  prog_lib.run_optimization_step.restype = ctypes.POINTER(ctypes.c_float)
  rets = prog_lib.run_optimization_step(argc, argv)
  size = int(round(np.ctypeslib.as_array(rets, shape=(1,))[0]))

  sim_trajectory_in = np.frombuffer((ctypes.c_float * (size + 1)).from_address(ctypes.addressof(rets.contents)), np.float32).copy()
  return sim_trajectory_in[1:]

def pix2m(vec,ratio,frame_size):
  """ Vec is n by 4 """
  vec_m = np.empty(vec.shape)
  vec_m[:,0] = (vec[:,0] - 0.5*frame_size[0])/ratio
  vec_m[:,1] = (frame_size[1] - vec[:,1])/ratio
  vec_m[:,2] = vec[:,2]*(1.0/0.03)/(ratio*(1.0/30.0))
  vec_m[:,3] = -vec[:,3]*(1.0/0.03)/(ratio*(1.0/30.0))
  return vec_m

def normalize_trajectory(vec,lb,ub):
  vec_norm = np.empty(vec.shape)
  vec_norm[:,0] = (vec[:,0]-lb)/(ub-lb)
  vec_norm[:,1] = (vec[:,1]-lb)/(ub-lb)
  return vec_norm

def normalize_params(vec,minx,maxx,lb,ub):
  vec_norm = np.empty(vec.shape)
  if len(vec.shape) > 1:
    for i in range(vec_norm.shape[1]):
      vec_norm[:,i] = ((ub-lb)*(vec[:,i]-minx)/(maxx-minx)) + lb

  else:
    vec_norm[:] = ((ub-lb)*(vec[:]-minx)/(maxx-minx)) + lb
  return vec_norm

def cost_function():
  n = real_trajectory.shape[0]
  w = np.diagflat([0.25,0.25,0.25,0.25])

  cost =np.sum(((sim_trajectory - real_trajectory)@w)@np.transpose(sim_trajectory - real_trajectory))/(2*n)
  return cost


def get_bounds():
  bounds = np.array([[-1200, -500], [0, 0.001], [0, 0.5]])
  # for i in range(0,track_ids.shape[0]):
  #   bounds = np.vstack((bounds,[-0.1,0.1],[-0.1,0.1],[-.1,.1]))
  return bounds


def get_bounds_norm():
  bounds_norm = np.array([[0, 1]])
  for i in range(0,x0.shape[0]-1):
    bounds_norm = np.vstack((bounds_norm,[0,1]))
  return bounds_norm

def fdlib(*args):
  return f(args)

def f(x, *args):
  x_scaled = normalize_params(x,bounds_norm[:,0],bounds_norm[:,1],bounds[:,0],bounds[:,1])

  global demo_path
  global sim_trajectory
  sim_trajectory = np.empty((0,4),dtype=float)
  for i in range(len(demo_paths)):
    demo_path = os.path.join(demo_path_prefix,demo_paths[i]+"/")
    sim_trajectory_n = run_prog_lib([demo_path]+list(map(str, x_scaled)))

    sim_trajectory_n = np.reshape(sim_trajectory_n, (len(sim_trajectory_n) // 4,4))
    sim_trajectory = np.vstack((sim_trajectory,sim_trajectory_n))

  cost = cost_function()

  return cost

def method_basinhopping():
  class MyBounds(object):
    def __init__(self, xmax=[1.1, 1.1], xmin=[-1.1, -1.1] ):
      self.xmax = np.array(xmax)
      self.xmin = np.array(xmin)

    def __call__(self, **kwargs):
      x = kwargs["x_new"]
      tmax = bool(np.all(x <= self.xmax))
      tmin = bool(np.all(x >= self.xmin))
      return tmax and tmin

  randn = np.random.random_sample(len(bounds_norm))
  x0 = np.array([x[0] + a*(x[1]-x[0]) for x,a in zip(bounds_norm, randn) ])
  basin_bounds = MyBounds([x[1] for x in bounds_norm], [x[0] for x in bounds_norm])
  result = basinhopping(f, x0, accept_test=basin_bounds, niter=args.opt_iters, disp=True)
  return result.fun, result.x

def method_differential_evolution():
  de_xo = np.vstack((x0.transpose(),get_random_x0()))
  for i in range(0,100-de_xo.shape[0]):
    de_xo = np.vstack((de_xo,get_random_x0()))
  result = differential_evolution(f, bounds_norm,init=de_xo,popsize=100,maxiter=args.opt_iters, disp=True,workers=-1,callback=prog_callback)
  return result.fun, result.x

def get_x0():
  x0 = np.zeros((3,1))
  x0[0] =-500
  x0[1] =.001
  x0[2] =0.3
  return x0

def prog_callback(res,convergence):
  global step_costs
  step_costs.append(res)

  return False

def get_random_x0():
  randn = np.random.random_sample(len(bounds_norm))
  rand_x0 = np.array([x[0] + a*(x[1]-x[0]) for x,a in zip(bounds_norm, randn) ])
  # rand_x0[4:] = 0.0
  return rand_x0

def _method_BFGS(eps):
  result = scipy.optimize.minimize(f, get_random_x0(), method='L-BFGS-B',bounds=bounds_norm, options={'disp' : True, 'eps' : eps })
  return result.fun, result.x

def method_BFGS_low():
  return _method_cg_eps(1e-5)

def method_BFGS_high():
  return _method_cg_eps(0.01)

def _method_TNC(eps):
  result = scipy.optimize.minimize(f, get_random_x0(), method='TNC',bounds=bounds_norm, options={'disp' : True, 'eps' : eps })
  return result.fun, result.x

def method_TNC_low():
  return _method_cg_eps(1e-5)

def method_TNC_high():
  return _method_cg_eps(0.01)

def _method_cg_eps(eps):
  result = scipy.optimize.minimize(f, get_random_x0(), method='CG', options={'disp' : True, 'eps' : eps })
  return result.fun, result.x

def method_cg_eps_low():
  return _method_cg_eps(1e-5)

def method_cg_eps_high():
  return _method_cg_eps(0.01)

def method_slsqp_high():
  result = scipy.optimize.minimize(f, get_random_x0(), method='SLSQP', bounds=bounds_norm,options={'disp' : True, 'eps' : 0.01 })
  return result.fun, result.x

def method_slsqp_low():
  result = scipy.optimize.minimize(f, get_random_x0(), method='SLSQP', bounds=bounds_norm, options={'disp' : True, 'eps' : 1e-5})
  return result.fun, result.x

def method_random():
  best = 1e20
  best_x = None
  for i in range(args.opt_iters):
    x0 = get_random_x0()
    cost = f(x0)

    if cost < best:
      best = cost
      best_x = x0

  return best, best_x

def method_dlib():
  import dlib
  x = dlib.find_min_global(fdlib, *list(map(list, zip(*bounds_norm))), args.opt_iters)
  return x[1], x[0]

def method_cma():
  import cma
  es = cma.fmin(f, x0[:,0], 0.25, options={
                              'verb_log': 0,
                              'popsize': 50,
                              #'tolfun': 1e-2,
                              'maxfevals': args.opt_iters,
                              #'tolx' : 1e-3,
                              'bounds': [[x[0] for x in bounds_norm], [x[1] for x in bounds_norm ] ]},
                              restarts=0)
  return es[1], es[0]

def run_n(f_method, n):
  costs = []
  times = []
  xs = []
  sim_trajs = []
  global step_costs
  global Nfval
  exp_step_costs = []
  for i in range(n):
    step_costs = []
    Nfval = 0

    t = time.time()
    cost, x = f_method()
    times.append(time.time() - t)
    costs.append(cost)
    xs.append(x)
    f(x)
    sim_trajs.append(sim_trajectory)
    exp_step_costs.append(step_costs)


  costs = np.array(costs)
  xs = np.array(xs)

  return costs, xs, sim_trajs, exp_step_costs

if __name__ == "__main__":
  import argparse

  global demo_path_prefix
  global demo_path 
  global demo_paths
  global real_trajectory
  global x0
  global bounds
  global bounds_norm  

  def get_map(prefix):
    return {x[len(prefix):] : y for x, y in globals().items() if x.startswith(prefix)}

  method_map = get_map('method_')
  types= {'slide':0,'fall':1,'bounce':2,'all':3}
  parser = argparse.ArgumentParser()
  parser.add_argument("type",default="all", type=str,nargs='?',choices=types.keys())
  parser.add_argument("method", default="random", type=str, nargs='?', choices=method_map.keys())
  parser.add_argument("--opt_iters", default=100, type=int, nargs='?')
  parser.add_argument("--exp_iters", default=5, type=int, nargs='?')


  args = parser.parse_args()
  method = method_map[args.method]

  if args.type == 'all':
    demo_path_prefix = get_demo_path("MarbleRun/Data/demos/complete/")
  else:
    demo_path_prefix = get_demo_path("MarbleRun/Data/demos/"+args.type+"/")

  demo_paths = os.listdir(demo_path_prefix)
  
  pix2m_ratio = np.loadtxt(get_p2m_ratio_file(),dtype=float)
  demo_lengths = np.ones((len(demo_paths),1),dtype=int)
  real_trajectory = np.empty((0,4),dtype=float)

  to_remove = []
  for i in range(len(demo_paths)):
    demo_path = os.path.join(demo_path_prefix,demo_paths[i]+"/")
    frame_size_n = np.loadtxt(get_frame_size_file())
    tmp = frame_size_n[0]
    frame_size_n[0] = frame_size_n[1]
    frame_size_n[1] = tmp

    track_config_n = np.loadtxt(get_track_config_file(),dtype=float)
    track_scale_n = np.sum(track_config_n[:,1])/track_config_n.shape[0]
    pix2m_ratio_n = pix2m_ratio*track_scale_n

    real_trajectory_n  = np.loadtxt(get_traj_file(),dtype=float)
    if len(real_trajectory_n.shape) <2:
      print(demo_path)
      to_remove.append(demo_paths[i])
      continue


    real_trajectory_n = real_trajectory_n[1:, 0: 4]
    real_trajectory_n = pix2m(real_trajectory_n,pix2m_ratio_n,frame_size_n)
    demo_lengths[i] = real_trajectory_n.shape[0]
    real_trajectory = np.vstack((real_trajectory,real_trajectory_n))

  for rm in to_remove:
    demo_paths.remove(rm)

  x0 = get_x0()

  bounds = get_bounds()
  bounds_norm = get_bounds_norm()
  x0 = normalize_params(x0,bounds[:,0],bounds[:,1],bounds_norm[:,0],bounds_norm[:,1])

  costs, x_minima, sim_trajs, exp_step_costs = run_n(method, args.exp_iters)


  max_n = 0
  max_i = 0
  for i in range(len(exp_step_costs)):
    if (len(exp_step_costs[i])>max_n):
      max_n = len(exp_step_costs[i])
      max_i = i

  exp_step_costs_np = np.empty((len(exp_step_costs),max_n))
  for i in range(len(exp_step_costs)):
    if (i!=max_i):
      for k in range(0, max_n - len(exp_step_costs[i])):
        exp_step_costs[i].append(exp_step_costs[i][-1])

    exp_step_costs_np[i,:] = np.array(exp_step_costs[i])

  fig, ax = plt.subplots()
  step_n = np.linspace(0,max_n-1,max_n)
  plt.rc('axes', prop_cycle=cycler('color', ['c', 'm', 'y', 'k']) )

  for i in range(len(exp_step_costs)):
    if i == np.argmin(costs):
      ax.plot(step_n,exp_step_costs_np[i,:],label='n={0} (best)'.format(i))
    else:
      ax.plot(step_n, exp_step_costs_np[i, :], label='n={0}'.format(i))

  ax.legend()
  ax.set_xlabel('Iteration # ')
  ax.set_ylabel('Cost')
  ax.set_title('Differential Evolution Optimization Cost')
  plt.savefig('de_optim_results_{0}.jpg'.format(args.type))
  x_minima = normalize_params(x_minima.transpose(),bounds_norm[:,0],bounds_norm[:,1],bounds[:,0],bounds[:,1])
  best_x = x_minima[:,np.argmin(costs)].transpose()
  strres= list(map(str, best_x))
  print("Final costs are ", costs)
  print("Local minima are ", x_minima.transpose())
  print("Optimal parameters are", ' '.join(strres))
  print([demo_path_prefix] + list(map(str, best_x)))
  np.savetxt('de_optim_results_{0}_xmin.txt'.format(args.type),x_minima)
  np.savetxt('de_optim_results_{0}_costs.txt'.format(args.type), costs.transpose())
