/*
* Copyright (c) 2006-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

#ifndef MARBLE_RUN_DEMO_H
#define MARBLE_RUN_DEMO_H
#include "../Framework/Test.h"
#include <algorithm>
#include <numeric>
#include <time.h>
class MarbleRunDemo : public Test
{

			
public:
	std::string tracks_path = "/home/aallaire/marble_run_simulator/MarbleRun/Data/tracks/";
	std::vector<std::vector<b2Vec2>> track_points;
	std::vector<std::vector<b2Vec3>> track_tris;
	bool demo_active = false;
	b2PolygonShape ball_poly;
	b2Body* m_ball;
	std::vector<b2Body *> m_tracks;
	bool skip_contact = false;
	struct State_t {
		b2Vec2 p;
		b2Vec2 v;
		std::vector<ContactPoint> cps;
	} state;
	int step_i;

	MarbleRunDemo()
	{
		srand( (unsigned)time( NULL ) );
		load_tracks(tracks_path);
	}

	float RandomFloat(float a, float b) {

		float random = ((float) rand()) / (float) RAND_MAX;
		float diff = b - a;
		float r = random * diff;
		return a + r;
	}
	void Setup(Settings *settings) override {
		SetupWorld(settings);

		DemoData *data = settings->demo_data;

		// Ground body
		{
			// b2BodyDef bd;
			// m_groundBody= m_world->CreateBody(&bd);

			// b2EdgeShape shape;
			// shape.Set(b2Vec2(-0.5*data->frame_size.x, 0.0f), b2Vec2(0.5*data->frame_size.x,0.0));
			// m_groundBody->CreateFixture(&shape, 0.0f);
		}

		// create ball shape
		{
			b2Vec2 ball_vertices[24];
			float cur_ang = 0.0;
			for(int i = 0; i < 24; i++) {
				ball_vertices[i] = b2Vec2(data->ball_radius*cos(cur_ang), data->ball_radius*sin(cur_ang));
				cur_ang += 2.0f*b2_pi/24.0f;

			}
			ball_poly.Set(ball_vertices,24);

			b2BodyDef ball_bd;
			ball_bd.type = b2_dynamicBody;
			ball_bd.position.Set(settings->p0.x,settings->p0.y);
			ball_bd.angle = 0.0f;
			ball_bd.linearVelocity.Set(settings->v0.x,settings->v0.y);

			ball_bd.bullet  = true;

			b2FixtureDef ball_fd;
			ball_fd.shape =&ball_poly;
			ball_fd.density = 0.25f;
			ball_fd.friction = settings->friction;
			ball_fd.restitution = settings->rest;

			m_ball = m_world->CreateBody(&ball_bd);
			m_ball->CreateFixture(&ball_fd);
			
		}

		// create tracks
		{
			m_tracks.clear();
			b2Vec2 vertices[3];
			b2BodyDef bd;
			b2FixtureDef fd;
			for (int i = 0; i < data->track_config.size(); ++i)
			{	

				float dx = 0,dy = 0, da = 0;
				if(settings->track_dp0.size() > 0) {
					dx = settings->track_dp0[i].x;
					dy = settings->track_dp0[i].y;
					da = settings->track_dp0[i].z;
				}
				int id = data->track_config[i][id_i];

				bd.type = b2_kinematicBody;
				bd.position.Set(data->track_config[i][tx_i]+dx,data->track_config[i][ty_i]+dy);
				bd.angle = data->track_config[i][rt_i] + da;
	
				b2Body* track = m_world->CreateBody(&bd);

				for (int j = 0; j < track_tris[id].size(); j++) {
					b2PolygonShape triangle;
					vertices[0] = data->track_config[i][sc_i]*track_points[id][track_tris[id][j].x];
					vertices[1] = data->track_config[i][sc_i]*track_points[id][track_tris[id][j].y];
					vertices[2] = data->track_config[i][sc_i]*track_points[id][track_tris[id][j].z];
					triangle.Set(vertices, 3);

					fd.shape = &triangle;
					fd.density = 1.0f;
					fd.friction = 1.0f;
					fd.restitution = 0.0f;
					track->CreateFixture(&fd);
				}
				m_tracks.push_back(track);


			}
		}

		CorrectPenetratingCollisions(settings);

		b2Vec2 impulse = m_ball->GetMass() * settings->v0; //disregard time factor
		m_ball->ApplyLinearImpulse( impulse, m_ball->GetWorldCenter(),true);
		demo_active = true;	
		step_i=0;
	}

	void Step(Settings *settings) override
	{
		float32 timeStep = settings->hz > 0.0f ? 1.0f / settings->hz : float32(0.0f);
		if (settings->pause && !settings->singleStep){
			timeStep = 0.0f;
		} 

		m_world->SetAllowSleeping(settings->enableSleep);
		m_world->SetWarmStarting(settings->enableWarmStarting);
		m_world->SetContinuousPhysics(settings->enableContinuous);
		m_world->SetSubStepping(settings->enableSubStepping);

		m_pointCount = 0;
		state.cps.clear();
		m_world->Step(timeStep, settings->velocityIterations, settings->positionIterations);
		state.p = m_ball->GetPosition();
		state.v = m_ball->GetLinearVelocity();

		if (timeStep > 0.0f)
		{
			++m_stepCount;
		}

		// Track maximum profile times
		UpdateProfileTimes();

		// Draw world if GUI enabled
		DrawGUI(settings);

		// Update step, pos, and velocity
		if(!settings->pause){
			step_i++;
		} else if (settings->pause && settings->singleStep ) {
			step_i++;
			settings->singleStep = 0;
		}
	}


	void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override{
		const b2Manifold* manifold = contact->GetManifold();
		if (manifold->pointCount == 0)
		{
			return;
		}
 
		b2Fixture* fixtureA = contact->GetFixtureA();
		b2Fixture* fixtureB = contact->GetFixtureB();

		b2PointState state1[b2_maxManifoldPoints], state2[b2_maxManifoldPoints];
		b2GetPointStates(state1, state2, oldManifold, manifold);

		b2WorldManifold worldManifold;
		contact->GetWorldManifold(&worldManifold);

		for (int32 i = 0; i < manifold->pointCount && m_pointCount < k_maxContactPoints; ++i)
		{
	
			ContactPoint* cp = m_points + m_pointCount;
			cp->fixtureA = fixtureA;
			cp->fixtureB = fixtureB;
			b2Body *bodyA = fixtureA->GetBody();
			b2Body *bodyB = fixtureB->GetBody();
			int bodyA_id = 0;
			int bodyB_id = 0;
			if(bodyA == m_ball) {
				bodyA_id = -1;
				bodyB_id = get_track_id_from_body(bodyB);
			} else if (bodyB == m_ball){
				bodyB_id = -1;
				bodyA_id = get_track_id_from_body(bodyA);
			}
			cp->fA_id = bodyA_id;
			cp->fB_id = bodyB_id;
			cp->position = worldManifold.points[i];
			cp->normal = worldManifold.normal;
			cp->state = state2[i];
			cp->normalImpulse = manifold->points[i].normalImpulse;
			cp->tangentImpulse = manifold->points[i].tangentImpulse;
			cp->separation = worldManifold.separations[i];

			if (state2[i] == b2_addState)
			{
				cp->vA = bodyA->GetLinearVelocity();
				cp->vB = bodyB->GetLinearVelocity();
				cp->pA = bodyA->GetTransform().p;
				cp->pB = bodyB->GetTransform().p;
				b2Vec2 dp = state.p - m_ball->GetTransform().p;
				cp->toi = 0.5*((dp.x/state.v.x) + (dp.y/state.v.y)); 
				state.cps.push_back(*cp);
			}
			++m_pointCount;
		}

		contact->SetEnabled(!skip_contact);

	}

	int get_track_id_from_body(b2Body *body) {
		int id = -1;
		for (int i = 0; i < m_tracks.size(); i++) {
			if (body == m_tracks[i]) {
				id = i;
				break;
			}
		}
		return id;
	}

	void SetParams(Settings *settings) {
		m_world->SetGravity(b2Vec2(0.0f,settings->gravity));
		(m_ball->GetFixtureList())->SetFriction(settings->friction);
		(m_ball->GetFixtureList())->SetRestitution(settings->rest);
	}

	void SetBallPos(Settings *settings) {
		m_ball->SetTransform(settings->p0,0.0);

	}

	void SetBallVel(Settings *settings){
		b2Vec2 impulse = m_ball->GetMass() * settings->v0; //disregard time factor
		m_ball->ApplyLinearImpulse( impulse, m_ball->GetWorldCenter(),true);
	}

	void SetTrackPose(Settings *settings) {
		DemoData *data = settings->demo_data;
		if (settings->track_dp0.size()>0) {
			for (int i = 0; i < data->track_config.size(); ++i) {	
				b2Body* track = m_tracks[i];

				b2Vec2 new_pos =  b2Vec2(data->track_config[i][tx_i] + settings->track_dp0[i].x,data->track_config[i][ty_i] + settings->track_dp0[i].y);
				float new_angle = data->track_config[i][rt_i] + settings->track_dp0[i].z;
				track->SetTransform(new_pos,new_angle);
			}
		}
	}

	void CorrectPenetratingCollisions(Settings *settings) {
		b2Vec2 pos = m_ball->GetTransform().p;
		float angle = m_ball->GetAngle();
		skip_contact = true;
		// std::cout << "before correction: " <<m_ball->GetTransform().p.x << " " <<  m_ball->GetTransform().p.y <<" " << settings->v0.x << " " << settings->v0.x << std::endl;
		m_world->Step(0,settings->velocityIterations,settings->positionIterations);
		if(m_pointCount > 0 ) {

			float32 min_dist = 0;
			int min_dist_ind = -1;
			for (int k =0; k < m_pointCount; k++) {
				if (m_points[k].separation < min_dist) {
					min_dist = m_points[k].separation;
					min_dist_ind = k;
				}
			}
			if (min_dist < 0) {
				
				b2Vec2 cur_pos = pos + min_dist*m_points[min_dist_ind].normal;
				// std::cout << "normal: "<<min_dist*m_points[min_dist_ind].normal.x << " " << min_dist*m_points[min_dist_ind].normal.y << std::endl;

				// std::cout << "after correction: " << cur_pos.x << " " << cur_pos.y << std::endl;
				m_ball->SetTransform(cur_pos,angle);
				settings->p0 = cur_pos;

			}
		}

		skip_contact = false;
	}

	void SetupWorld(Settings *settings) {
		if(demo_active) {
			delete m_world;

		}	
		m_world = new b2World(b2Vec2(0.0f,settings->gravity));
		m_bomb = NULL;
		m_textLine = 30;
		m_mouseJoint = NULL;
		m_pointCount = 0;

		m_destructionListener.test = this;
		m_world->SetDestructionListener(&m_destructionListener);
		m_world->SetContactListener(this);
		m_world->SetDebugDraw(&g_debugDraw);
		
		m_bombSpawning = false;

		m_stepCount = 0;

		memset(&m_maxProfile, 0, sizeof(b2Profile));
		memset(&m_totalProfile, 0, sizeof(b2Profile));
	}

	void UpdateProfileTimes() {
		const b2Profile& p = m_world->GetProfile();
		m_maxProfile.step = b2Max(m_maxProfile.step, p.step);
		m_maxProfile.collide = b2Max(m_maxProfile.collide, p.collide);
		m_maxProfile.solve = b2Max(m_maxProfile.solve, p.solve);
		m_maxProfile.solveInit = b2Max(m_maxProfile.solveInit, p.solveInit);
		m_maxProfile.solveVelocity = b2Max(m_maxProfile.solveVelocity, p.solveVelocity);
		m_maxProfile.solvePosition = b2Max(m_maxProfile.solvePosition, p.solvePosition);
		m_maxProfile.solveTOI = b2Max(m_maxProfile.solveTOI, p.solveTOI);
		m_maxProfile.broadphase = b2Max(m_maxProfile.broadphase, p.broadphase);

		m_totalProfile.step += p.step;
		m_totalProfile.collide += p.collide;
		m_totalProfile.solve += p.solve;
		m_totalProfile.solveInit += p.solveInit;
		m_totalProfile.solveVelocity += p.solveVelocity;
		m_totalProfile.solvePosition += p.solvePosition;
		m_totalProfile.solveTOI += p.solveTOI;
		m_totalProfile.broadphase += p.broadphase;
	}

	void DrawGUI(Settings *settings) {
		if (settings->doGUI) {
			if (settings->pause){
				g_debugDraw.DrawString(5, m_textLine, "****PAUSED****");
				m_textLine += DRAW_STRING_NEW_LINE;			
			}
			uint32 flags = 0;
			flags += settings->drawShapes			* b2Draw::e_shapeBit;
			flags += settings->drawJoints			* b2Draw::e_jointBit;
			flags += settings->drawAABBs			* b2Draw::e_aabbBit;
			flags += settings->drawCOMs				* b2Draw::e_centerOfMassBit;
			g_debugDraw.SetFlags(flags);

			m_world->DrawDebugData();
			g_debugDraw.Flush();

			if (settings->drawStats)
			{
				int32 bodyCount = m_world->GetBodyCount();
				int32 contactCount = m_world->GetContactCount();
				int32 jointCount = m_world->GetJointCount();
				g_debugDraw.DrawString(5, m_textLine, "bodies/contacts/joints = %d/%d/%d", bodyCount, contactCount, jointCount);
				m_textLine += DRAW_STRING_NEW_LINE;

				int32 proxyCount = m_world->GetProxyCount();
				int32 height = m_world->GetTreeHeight();
				int32 balance = m_world->GetTreeBalance();
				float32 quality = m_world->GetTreeQuality();
				g_debugDraw.DrawString(5, m_textLine, "proxies/height/balance/quality = %d/%d/%d/%g", proxyCount, height, balance, quality);
				m_textLine += DRAW_STRING_NEW_LINE;
			}

			if ( settings->drawProfile)
			{
				const b2Profile& p = m_world->GetProfile();

				b2Profile aveProfile;
				memset(&aveProfile, 0, sizeof(b2Profile));
				if (m_stepCount > 0)
				{
					float32 scale = 1.0f / m_stepCount;
					aveProfile.step = scale * m_totalProfile.step;
					aveProfile.collide = scale * m_totalProfile.collide;
					aveProfile.solve = scale * m_totalProfile.solve;
					aveProfile.solveInit = scale * m_totalProfile.solveInit;
					aveProfile.solveVelocity = scale * m_totalProfile.solveVelocity;
					aveProfile.solvePosition = scale * m_totalProfile.solvePosition;
					aveProfile.solveTOI = scale * m_totalProfile.solveTOI;
					aveProfile.broadphase = scale * m_totalProfile.broadphase;
				}

				g_debugDraw.DrawString(5, m_textLine, "step [ave] (max) = %5.2f [%6.2f] (%6.2f)", p.step, aveProfile.step, m_maxProfile.step);
				m_textLine += DRAW_STRING_NEW_LINE;
				g_debugDraw.DrawString(5, m_textLine, "collide [ave] (max) = %5.2f [%6.2f] (%6.2f)", p.collide, aveProfile.collide, m_maxProfile.collide);
				m_textLine += DRAW_STRING_NEW_LINE;
				g_debugDraw.DrawString(5, m_textLine, "solve [ave] (max) = %5.2f [%6.2f] (%6.2f)", p.solve, aveProfile.solve, m_maxProfile.solve);
				m_textLine += DRAW_STRING_NEW_LINE;
				g_debugDraw.DrawString(5, m_textLine, "solve init [ave] (max) = %5.2f [%6.2f] (%6.2f)", p.solveInit, aveProfile.solveInit, m_maxProfile.solveInit);
				m_textLine += DRAW_STRING_NEW_LINE;
				g_debugDraw.DrawString(5, m_textLine, "solve velocity [ave] (max) = %5.2f [%6.2f] (%6.2f)", p.solveVelocity, aveProfile.solveVelocity, m_maxProfile.solveVelocity);
				m_textLine += DRAW_STRING_NEW_LINE;
				g_debugDraw.DrawString(5, m_textLine, "solve position [ave] (max) = %5.2f [%6.2f] (%6.2f)", p.solvePosition, aveProfile.solvePosition, m_maxProfile.solvePosition);
				m_textLine += DRAW_STRING_NEW_LINE;
				g_debugDraw.DrawString(5, m_textLine, "solveTOI [ave] (max) = %5.2f [%6.2f] (%6.2f)", p.solveTOI, aveProfile.solveTOI, m_maxProfile.solveTOI);
				m_textLine += DRAW_STRING_NEW_LINE;
				g_debugDraw.DrawString(5, m_textLine, "broad-phase [ave] (max) = %5.2f [%6.2f] (%6.2f)", p.broadphase, aveProfile.broadphase, m_maxProfile.broadphase);
				m_textLine += DRAW_STRING_NEW_LINE;
			}

			if ( m_mouseJoint)
			{
				b2Vec2 p1 = m_mouseJoint->GetAnchorB();
				b2Vec2 p2 = m_mouseJoint->GetTarget();

				b2Color c;
				c.Set(0.0f, 1.0f, 0.0f);
				g_debugDraw.DrawPoint(p1, 4.0f, c);
				g_debugDraw.DrawPoint(p2, 4.0f, c);

				c.Set(0.8f, 0.8f, 0.8f);
				g_debugDraw.DrawSegment(p1, p2, c);
			}
			
			if ( m_bombSpawning)
			{
				b2Color c;
				c.Set(0.0f, 0.0f, 1.0f);
				g_debugDraw.DrawPoint(m_bombSpawnPoint, 4.0f, c);

				c.Set(0.8f, 0.8f, 0.8f);
				g_debugDraw.DrawSegment(m_mouseWorld, m_bombSpawnPoint, c);
			}
			

			if (settings->drawContactPoints)
			{
				const float32 k_impulseScale = 0.1f;
				const float32 k_axisScale = 0.3f;

				for (int32 i = 0; i < m_pointCount; ++i)
				{
					ContactPoint* point = m_points + i;

					if (point->state == b2_addState)
					{
						// Add
						g_debugDraw.DrawPoint(point->position, 10.0f, b2Color(0.3f, 0.95f, 0.3f));
					}
					else if (point->state == b2_persistState)
					{
						// Persist
						g_debugDraw.DrawPoint(point->position, 5.0f, b2Color(0.3f, 0.3f, 0.95f));
					}

					if (settings->drawContactNormals == 1)
					{
						b2Vec2 p1 = point->position;
						b2Vec2 p2 = p1 + k_axisScale * point->normal;
						g_debugDraw.DrawSegment(p1, p2, b2Color(0.9f, 0.9f, 0.9f));
					}
					else if (settings->drawContactImpulse == 1)
					{
						b2Vec2 p1 = point->position;
						b2Vec2 p2 = p1 + k_impulseScale * point->normalImpulse * point->normal;
						g_debugDraw.DrawSegment(p1, p2, b2Color(0.9f, 0.9f, 0.3f));
					}

					if (settings->drawFrictionImpulse == 1)
					{
						b2Vec2 tangent = b2Cross(point->normal, 1.0f);
						b2Vec2 p1 = point->position;
						b2Vec2 p2 = p1 + k_impulseScale * point->tangentImpulse * tangent;
						g_debugDraw.DrawSegment(p1, p2, b2Color(0.9f, 0.9f, 0.3f));
					}
				}
			}
		}
	}



	void load_tracks(std::string tracks_path) {
		std::vector<std::string> track_files = listFiles(tracks_path,".txt");
		track_points = std::vector<std::vector<b2Vec2>>(track_files.size()/2);
		track_tris = std::vector<std::vector<b2Vec3>> (track_files.size()/2);
		for (int i = 0; i < track_files.size(); i++) {
			int id = stoi(track_files[i].substr(track_files[i].find("0"),3));
			std::ifstream file(tracks_path + track_files[i]);
			std::string line;
			if(track_files[i].find("_tri")== std::string::npos) {
				std::vector<b2Vec2> track_points_vec;
				while(getline(file,line)) {
					float x = stof(line.substr(0,line.find(" ")));
					float y = stof(line.substr(line.find(" ")+1,line.length()-line.find(" ")));
					track_points_vec.push_back(b2Vec2(x,y));
				}
				track_points[id] = track_points_vec;
			} else {
				std::vector<b2Vec3> track_tris_vec;
				while(getline(file,line)) {
					float x = stof(line.substr(0,line.find(" ")));
					std::string line_rem = line.substr(line.find(" ")+1,line.length()-line.find(" "));
					float y = stof(line_rem.substr(0,line_rem.find(" ")));
					line_rem = line_rem.substr(line_rem.find(" ")+1,line_rem.length()-line_rem.find(" "));
					float z = stof(line_rem.substr(0,line_rem.find(" ")));
					track_tris_vec.push_back(b2Vec3(x,y,z));
				}
				track_tris[id] = track_tris_vec;
			}

			file.close();
		}
	}


	// vector returns (x,y,ind), where ind is wrt track_config
	std::vector<b2Vec3> get_candidate_start_pos(std::vector<std::vector<float>> track_config) {
		int num_tracks = track_config.size();
		
		// only want to start at top face of track (above the track's x axis)

		// 1. sort tracks by y pos (world coord, descending)
		// 2. pick top 2 tracks (if more than 3 tracks total) - if 3 tracks, then only pick top track
		// 3. sample candidate start points from top face of highest track(s)
		std::vector<b2Transform> track_transforms;
		std::vector<std::vector<b2Vec2>> local_points_above_x;
		std::vector<std::vector<b2Vec2>> world_points_above_x;
		std::vector<float> highest_ys;
		for(int i =0; i < num_tracks; i++) {
			// extract track pos, orientation
			int id = track_config[i][id_i];
			b2Vec2 pos = b2Vec2(track_config[i][tx_i],track_config[i][ty_i]);
			b2Rot rot = b2Rot(track_config[i][rt_i]);
			// create b2Transform object
			b2Transform this_transform = b2Transform(pos,rot);
			track_transforms.push_back(this_transform);

			// loop through points, save local point, 
			int num_points = track_points[id].size();
			std::vector<b2Vec2> this_lp_above_x;
			std::vector<b2Vec2> this_wp_above_x;
			float max_y = 0;
			int max_ind = 0;
			for(int j = 0; j < num_points; j++) {
				b2Vec2 point = track_points[id][j];
				bool above_x = point.y > .5;
				if (above_x) {
					// store local and world coordinates for point
					this_lp_above_x.push_back(point);
					b2Vec2 point_tx = b2Mul(this_transform,point);
					this_wp_above_x.push_back(point_tx);
					// track point with max y pos
					if (point_tx.y > max_y) {
						max_y = point_tx.y;
						max_ind = j;
					}
				}
			}
			// save local and world points (above x) for track
			local_points_above_x.push_back(this_lp_above_x);
			world_points_above_x.push_back(this_wp_above_x);
			// save max y pos
			highest_ys.push_back(max_y);
		}
		
		// sort tracks by y pos and pick top track(s)
		std::vector<int> cand_track_inds;
		std::vector<int> tracks_inds_sorted = sort_indexes_desc(highest_ys);
		cand_track_inds.push_back(tracks_inds_sorted[0]);
		if(num_tracks > 3) {
			cand_track_inds.push_back(tracks_inds_sorted[1]);
		}

		// all points above y may not be on top face, so find top-face corners
		// search for point farthest from origin on left and right separately.
		std::vector<b2Vec3> cand_start_pos;
		for (int i = 0; i< cand_track_inds.size(); i++) {
			int tl_ind = 0;
			int tr_ind = 0;
			float tl_dist = 0;
			float tr_dist = 0;

			int ind = cand_track_inds[i];
			std::cout << ind << std::endl;
			std::vector<int> idx(local_points_above_x[ind].size());
			std::vector<b2Vec2> points = sort_points_by_x(local_points_above_x[ind],idx);

			std::vector<b2Vec2> points_world(points.size());
			for (int j = 0; j < points.size(); j++) {
				points_world[j] = world_points_above_x[ind][idx[j]] ;
			}
			int num_points =points.size();
			for (int j = 0; j < num_points; j++) {
				float dist = points[j].Length();
				bool left = points[j].x < 0;
				
				if (left && (dist > tl_dist)) {
					tl_ind = j;
					tl_dist = dist;
				} else if ((!left) && (dist > tr_dist)) {
					tr_ind = j;
					tr_dist = dist;
				}
			}

			float tr_x = points[tr_ind].x;
			float tl_x = points[tl_ind].x;
			// std::cout << tr_x << " " << tl_x << std::endl;
			// std::cout << points[tr_ind].y << " " << points[tl_ind].y << std::endl;
			std::vector<float> cand_x;
			std::vector<float> nn_dist;
			std::vector<b2Vec2> cand_points;
			for(int j = 0; j < 3; j++) {
				float rand_x = RandomFloat(tl_x,tr_x);
				cand_x.push_back(rand_x);
				nn_dist.push_back(abs(tr_x - tl_x));
				cand_points.push_back(b2Vec2(0,0));
			} 

			// interpolate edge to find y pos for rand x
			for(int j = 0; j < 3; j++) {
				std::vector<float> dists;
				for (int k = 0; k < num_points; k++) {			
					float dist = abs(points[k].x - cand_x[j]);
					dists.push_back(dist);
				}
				std::vector<int> sorted_inds = sort_indexes_asc(dists);
				b2Vec2 p0,p1;
				if (points[sorted_inds[0]].x < cand_x[j]) {
					p0 = points[sorted_inds[0]];
					p1 = points[sorted_inds[1]];
				} else {
					p0 = points[sorted_inds[1]];
					p1 = points[sorted_inds[0]];
				}
				b2Vec2 cand_point = b2Vec2(cand_x[j],(p0.y*(p1.x - cand_x[j]) + p1.y*(cand_x[j] - p0.x))/(p1.x - p0.x));
				std::cout << p0.x << " " << p0.y << std::endl;
				std::cout << p1.x << " " << p1.y << std::endl;
				cand_points[j] = b2Mul(track_transforms[ind],cand_point);
 				// std::cout << cand_points[j].x << " " << cand_points[j].y << std::endl;


			} 
			for(int j = 0; j < 3; j++) {
				cand_start_pos.push_back(b2Vec3(cand_points[j].x,cand_points[j].y,ind));

			}

		}

		return cand_start_pos;

	}

	std::vector<b2Vec2> sort_points_by_x(std::vector<b2Vec2> v, std::vector<int> &idx) {
		std::vector<b2Vec2> v_sorted(v.size());

		std::iota(idx.begin(), idx.end(), 0);
		// sort indexes based on comparing values in v
		// using std::stable_sort instead of std::sort
		// to avoid unnecessary index re-orderings
		// when v contains elements of equal values 
		std::stable_sort(idx.begin(),idx.end(),
			[&v](int i1, int i2) {return v[i1].x < v[i2].x;});
		for (int i = 0; i < v.size(); i++) {
			v_sorted[i] = v[idx[i]];
		}
		return v_sorted;
	}
	template <typename T>
	std::vector<int> sort_indexes_asc(std::vector<T> &v) {

		// initialize original index locations
		std::vector<int> idx(v.size());
		std::iota(idx.begin(), idx.end(), 0);

		// sort indexes based on comparing values in v
		// using std::stable_sort instead of std::sort
		// to avoid unnecessary index re-orderings
		// when v contains elements of equal values 
		std::stable_sort(idx.begin(), idx.end(),
			[&v](int i1, int i2) {return v[i1] < v[i2];});

		return idx;
	}

	template <typename T>
	std::vector<int> sort_indexes_desc(std::vector<T> &v) {

		// initialize original index locations
		std::vector<int> idx(v.size());
		std::iota(idx.begin(), idx.end(), 0);

		// sort indexes based on comparing values in v
		// using std::stable_sort instead of std::sort
		// to avoid unnecessary index re-orderings
		// when v contains elements of equal values 
		std::stable_sort(idx.begin(), idx.end(),
			[&v](int i1, int i2) {return v[i1] > v[i2];});

		return idx;
	}
		// need to find edges 
	static Test* Create()
	{
		return new MarbleRunDemo();
	}

};




#endif
