#include "DemoDataLoader.h"



DemoDataLoader::DemoDataLoader() {}

DemoDataLoader::DemoDataLoader(std::string demo_path,float p2m_ratio) {
    load(demo_path,p2m_ratio);
}


void DemoDataLoader::load() {
    data = new DemoData();
    float ball_radius_pix =load_ball_radius();
    b2Vec2 frame_size_pix =  load_frame_size();
    std::vector<std::vector<float>> track_config_pix = load_track_config();
    std::vector<std::vector<float>> ball_trajectory_pix = load_ball_trajectory();
        
    float scale = 0.0;
    int num_tracks = track_config_pix.size();
    for(int i=0; i<num_tracks;i++) scale += track_config_pix[i][sc_i];
    scale = scale/num_tracks;
    p2m_ratio *= scale;

    data->dt = dt;
    data->demo_path = demo_path;
    data->ball_radius = (1/p2m_ratio)*ball_radius_pix;
    data->frame_size = (1/p2m_ratio)*frame_size_pix;

    b2Vec2 p2m_shift = b2Vec2(-0.5*frame_size_pix.x,frame_size_pix.y);

    data->track_config.clear();
    b2Vec2 pos_m;
    for(int i = 0; i < num_tracks; i++) {
        data->track_config.push_back(track_config_pix[i]);

        pos_m = pix2m(b2Vec2(track_config_pix[i][tx_i],track_config_pix[i][ty_i]),p2m_shift);
        data->track_config[i][tx_i] = pos_m.x;
        data->track_config[i][ty_i] = pos_m.y;
        data->track_config[i][rt_i] *= b2_pi/180.0;

    }
    std::vector<std::vector<b2Vec3>> track_dp_pix = load_track_dp();

    data->track_dp.clear();
    
    b2Vec2 dpos_m;
    std::vector<b2Vec3> dpos_vec(num_tracks);
    for(int j = 0; j < ball_trajectory_pix.size(); j++) {
        for(int i = 0; i < num_tracks; i++) {
            dpos_m = pix2m(b2Vec2(track_dp_pix[i][j].x,track_dp_pix[i][j].y));
            dpos_vec[i] = b2Vec3(dpos_m.x,dpos_m.y,track_dp_pix[i][j].z*b2_pi/180.0);
        }
        data->track_dp.push_back(dpos_vec);
    }

    data->ball_trajectory.resize(ball_trajectory_pix.size(),std::vector<float>(ball_trajectory_pix[0].size()));

    int traj_size = ball_trajectory_pix.size();
    b2Vec2 vel_m, acc_m;
    for(int i = 0; i < traj_size; i++) {
        pos_m = pix2m(b2Vec2(ball_trajectory_pix[i][bx_i],ball_trajectory_pix[i][by_i]),p2m_shift);
        data->ball_trajectory[i][bx_i] = pos_m.x;
        data->ball_trajectory[i][by_i] = pos_m.y;    
    
        vel_m = pix2m(b2Vec2(ball_trajectory_pix[i][vx_i],ball_trajectory_pix[i][vy_i]));
        data->ball_trajectory[i][vx_i] = vel_m.x*vel_acc_dt/dt;
        data->ball_trajectory[i][vy_i] = vel_m.y*vel_acc_dt/dt;    

        acc_m = pix2m(b2Vec2(ball_trajectory_pix[i][ax_i],ball_trajectory_pix[i][ay_i]));
        data->ball_trajectory[i][ax_i] = acc_m.x*vel_acc_dt*vel_acc_dt/(dt*dt);
        data->ball_trajectory[i][ay_i] = acc_m.y*vel_acc_dt*vel_acc_dt/(dt*dt);  
    }
}

void DemoDataLoader::load(std::string demo_path,float p2m_ratio) {
    this->p2m_ratio = p2m_ratio;
    this->demo_path = demo_path;
    
    load();
}

float DemoDataLoader::load_ball_radius() {

    std::ifstream file (demo_path + "ball_radius.txt");
    std::string line;
    getline (file,line);
    
    float ball_radius = stof(line);
    file.close();
    return ball_radius;
}

b2Vec2 DemoDataLoader::load_frame_size() {
    std::ifstream file (demo_path + "frame_size.txt");
    std::string line;
    getline (file,line);

    float height = stof(line.substr(0,line.find(" ")));
    float width = stof(line.substr(line.find(" "),line.length()-line.find(" ")));
    b2Vec2 frame_size = b2Vec2(width,height);
    file.close();
    return frame_size;
}

std::vector<std::vector<float>> DemoDataLoader::load_track_config() {
    std::ifstream file (demo_path + "track_config.txt");
    std::string line;
    
    std::vector<float> id_sc_rot_pos(5);
    std::vector<std::vector<float>> demo_track_config;
    while (getline (file,line)) {  
        float id = stof(line.substr(0,line.find(" ")));
        std::string line_rem = line.substr(line.find(" ")+1,line.length()-line.find(" "));
        float sc = stof(line_rem.substr(0,line_rem.find(" ")));
        line_rem = line_rem.substr(line_rem.find(" ")+1,line_rem.length()-line_rem.find(" "));
        float rot  = stof(line_rem.substr(0,line_rem.find(" ")));
        line_rem = line_rem.substr(line_rem.find(" ")+1,line_rem.length()-line_rem.find(" "));
        float pix_x = stof(line_rem.substr(0,line_rem.find(" ")));
        line_rem = line_rem.substr(line_rem.find(" ")+1,line_rem.length()-line_rem.find(" "));
        float pix_y = stof(line_rem.substr(0,line_rem.find(" ")));

        id_sc_rot_pos[0] = id;
        id_sc_rot_pos[1] = sc;
        id_sc_rot_pos[2] = rot;
        id_sc_rot_pos[3] = pix_x; 
        id_sc_rot_pos[4] = pix_y; 

        demo_track_config.push_back(id_sc_rot_pos);
    }
    file.close();

    return demo_track_config;
}

std::vector<std::vector<b2Vec3>> DemoDataLoader::load_track_dp() {

    std::vector<std::string> contour_warp_files = listFiles(demo_path + "contour_warps/");

    std::vector<std::vector<b2Vec3>> track_dp;
    std::vector<b2Vec3> track_dp_i;
    std::vector<float32> warp(6);
    int count = 0;
    std::string line;

    for(int i = 0; i < data->track_config.size(); i++) {
        for (int j =0; j <data->track_config.size(); j++) {
            int id = stoi(contour_warp_files[j].substr(contour_warp_files[j].find("0"),3));

            if(id == data->track_config[i][id_i]) {
                std::ifstream file(demo_path + "contour_warps/" + contour_warp_files[j] );

                while(getline(file,line)) {
                    count =0;
                    while(strcmp(line.c_str(),"")==0) {
                        auto split_pos = line.find(" ");
                        warp[count] = stof(line.substr(0,split_pos));
                        line = line.substr(split_pos+1,line.length()-split_pos);
                        count++;
                    }
                    float32 da = atan2(warp[3],warp[0])*180.0/b2_pi;
                    float32 dx = warp[2];
                    float32 dy = warp[5];

                    track_dp_i.push_back(b2Vec3(dx,dy,da));
                }

                track_dp.push_back(track_dp_i);
                file.close();
                break;
            }
        }
    }
    return track_dp;
}

std::vector<std::vector<float>> DemoDataLoader::load_ball_trajectory() {

    std::ifstream file (demo_path + "ball_trajectory.txt");
    std::string line;

    std::vector<std::vector<float>> ball_trajectory;
    std::vector<float> pos_vel_acc(6);

    while(getline(file,line)) {
        int count = 0;
        while(line.find(" ")!= std::string::npos) {
            std::string front = line.substr(0,line.find(" "));
            pos_vel_acc[count] = stof(front);
            line = line.substr(line.find(" ")+1,line.length()-line.find(" "));
            count++;
        }
        pos_vel_acc[count]=stof(line);
        ball_trajectory.push_back(pos_vel_acc);
    }
    file.close();
    return ball_trajectory;
}

b2Vec2 DemoDataLoader::pix2m(b2Vec2 point_px) {
    b2Vec2 point_m;
    point_m.y = -point_px.y/p2m_ratio;
    point_m.x = point_px.x/p2m_ratio;
    return point_m;
}

b2Vec2 DemoDataLoader::pix2m(b2Vec2 point_px,b2Vec2 shift) {
    b2Vec2 point_m;
    point_m.y = (-point_px.y +shift.y)/p2m_ratio;
    point_m.x = (point_px.x +shift.x)/p2m_ratio;
    return point_m;
}