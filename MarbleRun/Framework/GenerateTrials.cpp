/*
* Copyright (c) 2006-2016 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

#if defined(__APPLE__)
#include <OpenGL/gl3.h>
#else
#include <glew/glew.h>
#endif

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"
#include "DebugDraw.h"
#include "Test.h"
#include "../Tests/MarbleRunDemo.h"
#include "DemoDataLoader.h"
#include "glfw/glfw3.h"
#include <stdio.h>



//
struct UIState
{
	bool showMenu;
};

//
namespace
{
	GLFWwindow* mainWindow = NULL;
	UIState ui;
    DemoData *demo_data;
	std::vector<std::string> demoDirs;
	std::string data_path = "/home/aallaire/marble_run_simulator/MarbleRun/Data/";
	std::string cand_trials_path = "/home/aallaire/marble_run_simulator/MarbleRun/Data/candidate_trials/";

	std::string p2m_ratio_path = data_path + "p2m_ratio.txt";
	float p2m_ratio;

	MarbleRunDemo* test;
	Settings settings;

	std::vector<b2Vec2> sim_trajectory;

	bool rightMouseDown;
	b2Vec2 lastp;
	int doGUI = true;
	int sim_over = true;
}

static void sCreateUI(GLFWwindow* window)
{
	ui.showMenu = true;

	// Init UI
	const char* fontPath = "/home/aallaire/marble_run_simulator/MarbleRun/Data/DroidSans.ttf";
	ImGui::GetIO().Fonts->AddFontFromFileTTF(fontPath, 15.f);

	if (ImGui_ImplGlfwGL3_Init(window, false) == false)
	{
		fprintf(stderr, "Could not init GUI renderer.\n");
		assert(false);
		return;
	}

	ImGuiStyle& style = ImGui::GetStyle();
	style.FrameRounding = style.GrabRounding = style.ScrollbarRounding = 2.0f;
	style.FramePadding = ImVec2(4, 2);
	style.DisplayWindowPadding = ImVec2(0, 0);
	style.DisplaySafeAreaPadding = ImVec2(0, 0);
}

//
static void sResizeWindow(GLFWwindow*, int width, int height)
{
	g_camera.m_width = width;
	g_camera.m_height = height;
}



static void sKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	ImGui_ImplGlfwGL3_KeyCallback(window, key, scancode, action, mods);
	bool keys_for_ui = ImGui::GetIO().WantCaptureKeyboard;
	if (keys_for_ui)
		return;

	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			// Quit
			glfwSetWindowShouldClose(mainWindow, GL_TRUE);
			break;

		case GLFW_KEY_LEFT:
			// Pan left
			if (mods == GLFW_MOD_CONTROL)
			{
				b2Vec2 newOrigin(2.0f, 0.0f);
				test->ShiftOrigin(newOrigin);
			}
			else
			{
				g_camera.m_center.x -= 0.5f;
			}
			break;

		case GLFW_KEY_RIGHT:
			// Pan right
			if (mods == GLFW_MOD_CONTROL)
			{
				b2Vec2 newOrigin(-2.0f, 0.0f);
				test->ShiftOrigin(newOrigin);
			}
			else
			{
				g_camera.m_center.x += 0.5f;
			}
			break;

		case GLFW_KEY_DOWN:
			// Pan down
			if (mods == GLFW_MOD_CONTROL)
			{
				b2Vec2 newOrigin(0.0f, 2.0f);
				test->ShiftOrigin(newOrigin);
			}
			else
			{
				g_camera.m_center.y -= 0.5f;
			}
			break;

		case GLFW_KEY_UP:
			// Pan up
			if (mods == GLFW_MOD_CONTROL)
			{
				b2Vec2 newOrigin(0.0f, -2.0f);
				test->ShiftOrigin(newOrigin);
			}
			else
			{
				g_camera.m_center.y += 0.5f;
			}
			break;

		case GLFW_KEY_HOME:
			// Reset view
			g_camera.m_zoom = 1.0f;
			g_camera.m_center.Set(0.0f, 20.0f);
			break;

		case GLFW_KEY_Z:
			// Zoom out
			g_camera.m_zoom = b2Min(1.1f * g_camera.m_zoom, 20.0f);
			break;

		case GLFW_KEY_X:
			// Zoom in
			g_camera.m_zoom = b2Max(0.9f * g_camera.m_zoom, 0.02f);
			break;

		case GLFW_KEY_R:
			// Reset test
			settings.p0 = b2Vec2(settings.demo_data->ball_trajectory[settings.step_i][bx_i],settings.demo_data->ball_trajectory[settings.step_i][by_i]);
			settings.v0 = b2Vec2(settings.demo_data->ball_trajectory[settings.step_i][vx_i],settings.demo_data->ball_trajectory[settings.step_i][vy_i]);
			test->Setup(&settings);
			sim_over = false;
			break;

		case GLFW_KEY_SPACE:
			// Launch a bomb.
			if (test)
			{
				test->LaunchBomb();
			}
			break;

		case GLFW_KEY_O:
			settings.singleStep = true;
			break;

		case GLFW_KEY_P:
			if(settings.simSingleStep) settings.pause = true;
			else	settings.pause = !settings.pause;
			break;
		case GLFW_KEY_TAB:
			ui.showMenu = !ui.showMenu;

		default:
			if (test)
			{
				test->Keyboard(key);
			}
		}
	}
	else if (action == GLFW_RELEASE)
	{
		test->KeyboardUp(key);
	}
	// else GLFW_REPEAT
}

//
static void sCharCallback(GLFWwindow* window, unsigned int c)
{
	ImGui_ImplGlfwGL3_CharCallback(window, c);
}

//
static void sMouseButton(GLFWwindow* window, int32 button, int32 action, int32 mods)
{
	ImGui_ImplGlfwGL3_MouseButtonCallback(window, button, action, mods);

	double xd, yd;
	glfwGetCursorPos(mainWindow, &xd, &yd);
	b2Vec2 ps((float32)xd, (float32)yd);

	// Use the mouse to move things around.
	if (button == GLFW_MOUSE_BUTTON_1)
	{
        //<##>
        //ps.Set(0, 0);
		b2Vec2 pw = g_camera.ConvertScreenToWorld(ps);
		if (action == GLFW_PRESS)
		{
			if (mods == GLFW_MOD_SHIFT)
			{
				test->ShiftMouseDown(pw);
			}
			else
			{
				test->MouseDown(pw);
			}
		}
		
		if (action == GLFW_RELEASE)
		{
			test->MouseUp(pw);
		}
	}
	else if (button == GLFW_MOUSE_BUTTON_2)
	{
		if (action == GLFW_PRESS)
		{	
			lastp = g_camera.ConvertScreenToWorld(ps);
			rightMouseDown = true;
		}

		if (action == GLFW_RELEASE)
		{
			rightMouseDown = false;
		}
	}
}

//
static void sMouseMotion(GLFWwindow*, double xd, double yd)
{
	b2Vec2 ps((float)xd, (float)yd);

	b2Vec2 pw = g_camera.ConvertScreenToWorld(ps);
	test->MouseMove(pw);
	
	if (rightMouseDown)
	{
		b2Vec2 diff = pw - lastp;
		g_camera.m_center.x -= diff.x;
		g_camera.m_center.y -= diff.y;
		lastp = g_camera.ConvertScreenToWorld(ps);
	}
}

//
static void sScrollCallback(GLFWwindow* window, double dx, double dy)
{
	ImGui_ImplGlfwGL3_ScrollCallback(window, dx, dy);
	bool mouse_for_ui = ImGui::GetIO().WantCaptureMouse;

	if (!mouse_for_ui)
	{
		if (dy > 0)
		{
			g_camera.m_zoom /= 1.1f;
		}
		else
		{
			g_camera.m_zoom *= 1.1f;
		}
	}
}

//
static void sRestart()
{
	settings.p0 = b2Vec2(settings.demo_data->ball_trajectory[settings.step_i][bx_i],settings.demo_data->ball_trajectory[settings.step_i][by_i]);
	settings.v0 = b2Vec2(settings.demo_data->ball_trajectory[settings.step_i][vx_i],settings.demo_data->ball_trajectory[settings.step_i][vy_i]);

	test->Setup(&settings);
	sim_over = false;

}

//
static void sSimulate()
{
	glEnable(GL_DEPTH_TEST);
	test->Step(&settings);
	test->DrawTitle("Generate Trials");
	glDisable(GL_DEPTH_TEST);
}

//
static void sInterface()
{
	int menuWidth = 300;
	if (ui.showMenu)
	{
		ImGui::SetNextWindowPos(ImVec2((float)g_camera.m_width - menuWidth - 10, 10));
		ImGui::SetNextWindowSize(ImVec2((float)menuWidth, (float)g_camera.m_height - 20));
		ImGui::Begin("Testbed Controls", &ui.showMenu, ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoCollapse);
		ImGui::PushAllowKeyboardFocus(false); // Disable TAB

		ImGui::PushItemWidth(-1.0f);

		
		ImGui::Text("Vel Iters");
		ImGui::SliderInt("##Vel Iters", &settings.velocityIterations, 0, 50);
		ImGui::Text("Pos Iters");
		ImGui::SliderInt("##Pos Iters", &settings.positionIterations, 0, 50);
		ImGui::Text("Sim Hertz");
		ImGui::SliderFloat("##Sim Hertz", &settings.hz, 5.0f, 120.0f, "%.0f hz");
		ImGui::PopItemWidth();

		ImGui::Separator();

		ImGui::Checkbox("Shapes", &settings.drawShapes);
		ImGui::Checkbox("Joints", &settings.drawJoints);
		ImGui::Checkbox("AABBs", &settings.drawAABBs);
		ImGui::Checkbox("Contact Points", &settings.drawContactPoints);
		ImGui::Checkbox("Contact Normals", &settings.drawContactNormals);
		ImGui::Checkbox("Contact Impulses", &settings.drawContactImpulse);
		ImGui::Checkbox("Friction Impulses", &settings.drawFrictionImpulse);
		ImGui::Checkbox("Center of Masses", &settings.drawCOMs);
		// ImGui::Checkbox("Statistics", &settings.drawStats);
		// ImGui::Checkbox("Profile", &settings.drawProfile);

		ImVec2 button_sz = ImVec2(-1, 0);
		if (ImGui::Button("Pause (P)", button_sz))
			if (settings.simSingleStep) settings.pause = true;
			else settings.pause = !settings.pause;

		if (ImGui::Button("Single Step (O)", button_sz))
			settings.singleStep = !settings.singleStep;

		if (ImGui::Button("Restart (R)", button_sz))
			sRestart();

		if (ImGui::Button("Quit", button_sz))
			glfwSetWindowShouldClose(mainWindow, GL_TRUE);

		ImGui::PopAllowKeyboardFocus();
		ImGui::End();
	}

	//ImGui::ShowTestWindow(NULL);
}

//
void glfwErrorCallback(int error, const char *description)
{
	fprintf(stderr, "GLFW error occured. Code: %d. Description: %s\n", error, description);
}

void load_p2m_ratio() {
    std::ifstream file(p2m_ratio_path);
    std::string line;
    getline (file,line);
    p2m_ratio = stof(line);
    file.close();
}

// TODO: load this from actual file
void load_demo_data(std::string filepath){
	settings.demo_data->ball_radius = 18.4;
	settings.demo_data->frame_size = b2Vec2(1920,1080);
	settings.demo_data->dt = 1.0/60.0;
	settings.demo_data->demo_path = filepath;
}

std::vector<std::vector<float>> load_candidate_trial(std::string filepath) {
    std::ifstream file (filepath);
    std::string line;
    
    std::vector<float> id_sc_rot_pos(5);
    std::vector<std::vector<float>> cand_track_config;
    while (getline (file,line)) {  
		// TODO: loop here 
        float id = stof(line.substr(0,line.find(" ")));
        std::string line_rem = line.substr(line.find(" ")+1,line.length()-line.find(" "));
        float pix_x = stof(line_rem.substr(0,line_rem.find(" ")));
        line_rem = line_rem.substr(line_rem.find(" ")+1,line_rem.length()-line_rem.find(" "));
        float pix_y = stof(line_rem.substr(0,line_rem.find(" ")));
        line_rem = line_rem.substr(line_rem.find(" ")+1,line_rem.length()-line_rem.find(" "));
        float rot = stof(line_rem.substr(0,line_rem.find(" ")));

        id_sc_rot_pos[0] = id;
		id_sc_rot_pos[1] = 1;
		id_sc_rot_pos[2] = rot*b2_pi/180.0;
        id_sc_rot_pos[3] = pix_x/p2m_ratio; 
        id_sc_rot_pos[4] = pix_y/p2m_ratio; 

        cand_track_config.push_back(id_sc_rot_pos);
    }
    file.close();

    return cand_track_config;
}

void init_trial_params(std::vector<std::vector<float>> cand_track_config,b2Vec2 pos ) {
	settings.track_dp0.clear();
	settings.demo_data->track_config.clear();
	settings.demo_data->track_config = cand_track_config;
	settings.p0 = pos;
	settings.v0 = b2Vec2_zero;
}



float simulate_trial(b2Vec3 start_pos, std::vector<std::vector<float>> trial_config) {
    init_trial_params(trial_config, b2Vec2(start_pos.x,start_pos.y) ) ;

	settings.gravity = -500;
	settings.friction = 0.000976;
	settings.rest = 0.3;
	settings.pause = true;
	settings.doGUI = true;

	test->Setup(&settings);
	sim_over = false;


	float xmin = -0.5*(settings.demo_data->frame_size.x);
	float xmax = -0.5*(settings.demo_data->frame_size.y);
	float ymin = 0;
	float ymax = settings.demo_data->frame_size.y;

	// Control the frame rate. One draw per monitor refresh.
	double time1;
	if(doGUI) {
		glfwSwapInterval(1);
		time1= glfwGetTime();
		glClearColor(0.3f, 0.3f, 0.3f, 1.f);
	}
	double frameTime = 0.0;

	while((settings.doGUI &&(!glfwWindowShouldClose(mainWindow))) ||  ((!settings.doGUI) && (!sim_over))) {
		if (settings.doGUI) {
			glfwGetWindowSize(mainWindow, &g_camera.m_width, &g_camera.m_height);
		
			int bufferWidth, bufferHeight;
			glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);
			glViewport(0, 0, bufferWidth, bufferHeight);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			ImGui_ImplGlfwGL3_NewFrame();
			ImGui::SetNextWindowPos(ImVec2(0,0));
			ImGui::SetNextWindowSize(ImVec2((float)g_camera.m_width, (float)g_camera.m_height));
			ImGui::Begin("Overlay", NULL, ImVec2(0,0), 0.0f, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoInputs|ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoScrollbar);
			ImGui::SetCursorPos(ImVec2(5, (float)g_camera.m_height - 20));
			ImGui::Text("%.1f ms", 1000.0 * frameTime);
			ImGui::End();

			if (!sim_over) {
				sSimulate();
			}
			sInterface();

			// Measure speed
			double time2 = glfwGetTime();
			double alpha = 0.9f;
			frameTime = alpha * frameTime + (1.0 - alpha) * (time2 - time1);
			time1 = time2;

			ImGui::Render();

			glfwSwapBuffers(mainWindow);

			glfwPollEvents();
		} else if (!sim_over) {
			test->Step(&settings);
		}

		bool out_of_frame = (test->state.p.x < xmin) || (test->state.p.x > xmax) ||  (test->state.p.y < ymin) || (test->state.p.y > ymax);
		bool stopped = sqrt((test->state.v.x*test->state.v.x) + (test->state.v.y*test->state.v.y)) < 1e-5;
		sim_over = out_of_frame || stopped;
		

	}

	if (test)
	{
		delete test;
		test = NULL;
	}


	return 0;
}
//
int main(int argc, char**argv)
{
	load_p2m_ratio();
	std::vector<std::string> candidate_trials = listFiles(cand_trials_path);
	load_demo_data(candidate_trials[0]);
	std::vector<std::vector<float>>  cand_track_config = load_candidate_trial(cand_trials_path + candidate_trials[0]);
	std::vector<b2Vec3>  pos = test->get_candidate_start_pos(cand_track_config);
	float rets = simulate_trial(pos[0],cand_track_config);

	return 0;
}
