#ifndef UTIL_H
#define UTIL_H

#include <dirent.h>
#include <vector>
#include <string.h>
#include <iostream>
#include <algorithm>
#include <sys/stat.h>


std::vector<std::string> listFiles(std::string filepath,std::string sub_str = "");
std::vector<std::string> listDirs(std::string filepath, std::string sub_str="");
bool path_exists(std::string path);
bool make_directory(std::string dir);
void remove_directory(std::string dir);


#endif