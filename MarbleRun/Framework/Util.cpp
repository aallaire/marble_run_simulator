#include "Util.h"

std::vector<std::string> listFiles(std::string filepath,std::string sub_str ){
        DIR *pDIR;
        struct dirent *entry;
        std::vector<std::string> files;
        if( pDIR=opendir(filepath.c_str()) ){
                while(entry = readdir(pDIR)){
                        if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 && (entry->d_type != DT_DIR) ) {
                            bool valid = true;

                            if(strcmp (sub_str.c_str(),"") != 0) {
                                std::string name(entry->d_name);
                                std::size_t found = name.find(sub_str);
                                valid = found!=std::string::npos;
                            } 
                            if (valid) {
                                std::string str(entry->d_name);
                                files.push_back( str);
                            }
                        }
                }
                closedir(pDIR);
        }
        sort(begin(files),end(files));
        return files;
}

std::vector<std::string> listDirs(std::string filepath, std::string sub_str){
        DIR *pDIR;
        struct dirent *entry;
        std::vector<std::string> files;
        if( pDIR=opendir(filepath.c_str()) ){
                while(entry = readdir(pDIR)){
                        if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 && (entry->d_type == DT_DIR) ) {
                            bool valid = true;

                            if(strcmp (sub_str.c_str(),"") != 0) {
                                std::string name(entry->d_name);
                                std::size_t found = name.find(sub_str);
                                valid = found!=std::string::npos;
                            } 
                            if (valid) {
                                std::string str(entry->d_name);
                                files.push_back( str + "/");
                            }
                        }
                }
                closedir(pDIR);
        }
        sort(begin(files),end(files));
        return files;
}

bool path_exists(std::string path) {
    struct stat st;
    return (stat(path.c_str(),&st) == 0) && (st.st_mode & S_IFDIR != 0);
}

bool make_directory(std::string dir) {
    if (path_exists(dir)) {
        return true;
    } else {
        mkdir(dir.c_str(),0777);
        return path_exists(dir);
    }

}

void remove_directory(std::string dir) {
    std::string cmd = "rm -rf ";
    cmd = cmd + dir;
    system(cmd.c_str());
}
