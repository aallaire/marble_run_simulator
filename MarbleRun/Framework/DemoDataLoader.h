
#ifndef DEMO_DATA_H
#define DEMO_DATA_H
#include "Util.h"
#include "Box2D/Box2D.h"
#include <fstream>

enum dd_config_i{id_i,sc_i,rt_i,tx_i,ty_i};
enum dd_traj_i{bx_i,by_i,vx_i,vy_i,ax_i,ay_i};

struct DemoData
{
	float dt;
	std::string demo_path;
	float ball_radius; 
	b2Vec2 frame_size; 
	std::vector<std::vector<float>> track_config;
	std::vector<std::vector<b2Vec3>> track_dp; // x,y,r
	std::vector<std::vector<float>> ball_trajectory;

};

class DemoDataLoader {
	
    public:
		DemoData *data;

		DemoDataLoader();
		DemoDataLoader(std::string,float p2m_ratio);


		void load(std::string,float p2m_ratio);

	private:
		float dt = 1.0/30.0;
		float vel_acc_dt = 1.0/0.03;
		float p2m_ratio;
		std::string demo_path;

		void load();
		float load_ball_radius();
		b2Vec2 load_frame_size();
		std::vector<std::vector<float>>  load_ball_trajectory();
		std::vector<std::vector<float>> load_track_config();
		std::vector<std::vector<b2Vec3>> load_track_dp();

		b2Vec2 pix2m(b2Vec2 point_px);
		b2Vec2 pix2m(b2Vec2 point_px, b2Vec2 shift);
};

#endif